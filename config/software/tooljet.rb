#
# Copyright 2023 YOUR NAME
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

name "tooljet"
description "tooljet codebase"
default_version "2.4.9"

license :project_license
skip_transitive_dependency_licensing true

source git: "https://github.com/tooljet/tooljet.git"
default_version "main"

dependency "postgresql"
dependency "nodejs"
dependency "python"


build do
  block do
    command "cp .env.example .env"
    command "npm install"
    command "npm install --prefix server"
    command "npm install --prefix frontend"
    command "npm run build:plugins"
    command "npm run --prefix server db:reset"
  end
  
end
