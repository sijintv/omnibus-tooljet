#
# Copyright 2023 YOUR NAME
#
# All Rights Reserved.
#

name "tooljet"
maintainer "Sijin T V"
homepage "https://tooljet.com"

# Defaults to C:/tooljet on Windows
# and /opt/tooljet on all other platforms
install_dir "#{default_root}/#{name}"

build_version Omnibus::BuildVersion.semver
build_iteration 1

# Creates required build directories
dependency "preparation"

# tooljet dependencies/components
dependency "tooljet"

exclude "**/.git"
exclude "**/bundler/git"
